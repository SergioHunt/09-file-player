//
//  DrumPad.hpp
//  JuceBasicAudio - App
//
//  Created by Sergio Hunt on 12/12/2017.
//

#ifndef DrumPad_hpp
#define DrumPad_hpp

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"

class DrumPad : public AudioSource
{
public:
    //**Constructor*/
    DrumPad();
    /**
     Destructor
     */
    ~DrumPad();
    
    
private:
};

#endif /* DrumPad_hpp */
