/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent (Audio& audio_) : audio (audio_),
filePlayerGui (audio_.getFilePlayer()), drumPadGui(audio_.getFilePlayer())

{
    
    setSize (900, 750);
    
    //Add Child Components
    addAndMakeVisible(filePlayerGui);
    addAndMakeVisible(drumPadGui);
    addAndMakeVisible(waveformGui);
    
    
    for (int n=1; n <= NumFilePlayers; n++) {
     //addAndMakeVisible(filePlayerGui);
        
    }
}

MainComponent::~MainComponent()
{

}

void MainComponent::resized()
{
    
    //FilePlayer
    filePlayerGui.setBounds (0, 0, getWidth(), 80);
    //DrumPads 
    drumPadGui.setBounds(120, 270, 360, 360);
    //Waveform
    waveformGui.setBounds(100, 80, 400, 150);
    
}

void MainComponent::paint(Graphics &g)
{
    g.setColour(Colours::grey);
    //left Border
    g.drawLine(0, 0, 0, 750, 50);
    //Right Border
    g.drawLine(900, 0, 900, 750, 50);
    //Bottom Bar
    g.fillRect(10, 650, 900, 100);
}

//MenuBarCallbacks==============================================================
StringArray MainComponent::getMenuBarNames()
{
    const char* const names[] = { "File", 0 };
    return StringArray (names);
}

PopupMenu MainComponent::getMenuForIndex (int topLevelMenuIndex, const String& menuName)
{
    PopupMenu menu;
    if (topLevelMenuIndex == 0)
        menu.addItem(AudioPrefs, "Audio Prefrences", true, false);
    return menu;
}

void MainComponent::menuItemSelected (int menuItemID, int topLevelMenuIndex)
{
    if (topLevelMenuIndex == FileMenu)
    {
        if (menuItemID == AudioPrefs)
        {
            AudioDeviceSelectorComponent audioSettingsComp (audio.getAudioDeviceManager(),
                                                            0, 2, 2, 2, true, true, true, false);
            audioSettingsComp.setSize (450, 350);
            DialogWindow::showModalDialog ("Audio Settings",
                                           &audioSettingsComp, this, Colours::lightgrey, true);
        }
    }
}

