//
//  VolumeGui.cpp
//  JuceBasicAudio - App
//
//  Created by Sergio Hunt on 28/12/2017.
//

#include "VolumeGui.hpp"


VolumeGui::VolumeGui()
{
    //Volume Slider
    masterVolume.setSliderStyle(Slider::Rotary);
    masterVolume.setRange (0.0,1.0);
    addAndMakeVisible(masterVolume);
    masterVolume.addListener(this);
    
    //Volume Slider
    recordVolume.setSliderStyle(Slider::Rotary);
    recordVolume.setRange (0.0,1.0);
    addAndMakeVisible(recordVolume);
    recordVolume.addListener(this);
}

VolumeGui::~VolumeGui()
{
    
}

//Component
void VolumeGui::resized()
{
    auto r = getLocalBounds();
    
}
void VolumeGui::paint (Graphics& g)
{
    
}

//Slider Listener
void VolumeGui::sliderValueChanged(Slider* slider)
{
    if (slider == &masterVolume)
    {
        DBG ("Eatshit");
       
    }
    else if (slider == &recordVolume)
    {
        DBG ("GetFucked");
    }
}
