//
//  DrumPadGui.hpp
//  JuceBasicAudio - App
//
//  Created by Sergio Hunt on 13/12/2017.
//

#ifndef DrumPadGui_hpp
#define DrumPadGui_hpp

#include "../JuceLibraryCode/JuceHeader.h"
#include "../audio/FilePlayer.h"
#include <stdio.h>

class DrumPadGui :  public Component,
                    public Button::Listener

{
public:
    /**
     constructor - receives a reference to a FilePlayer object to control
     */
    DrumPadGui(FilePlayer& filePlayer_);
    /**
     Destructor
     */
    ~DrumPadGui();
    
    //Component
    void resized() override;
    void paint (Graphics& g) override;
    
    //Button Listener
    void buttonClicked (Button* button)override ;
    
private:
    
    const int numbDrumPads = 16;
    TextButton drumPad[16];
    
    
    
};

#endif /* DrumPadGui_hpp */
