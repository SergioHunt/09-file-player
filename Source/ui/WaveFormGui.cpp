//
//  WaveFormGui.cpp
//  JuceBasicAudio - App
//
//  Created by Sergio Hunt on 13/12/2017.
//

#include "WaveFormGui.hpp"


WaveFormGui::WaveFormGui() : thumbnailCache (5),
                             thumbnail (512, formatManager, thumbnailCache)

{
    
DBG ("waveform construct");
    
}

WaveFormGui::~WaveFormGui()
{
    
}

void WaveFormGui::resized()
{
    auto r = getLocalBounds();
    //const int border = 10;
    
    //drumPad[0].setBounds(r.removeFromTop(border), r.removeFromLeft(border), 30, 30);
    //drumPad[0].setBounds(20,20,20,20);
    //drumPad[1].setBounds(0, 0, 20, 20);
}

void WaveFormGui::paint (Graphics& g)
{
   // DBG ("waveform Paint Function is being Called");
    
    auto r = getLocalBounds();
    
    g.setColour(Colours::grey);
    g.fillRect(getLocalBounds());
    
    //g.drawRect(120, 240, 380, 380);
    
    
}
