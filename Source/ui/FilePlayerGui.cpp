/*
  ==============================================================================

    FilePlayerGui.cpp
    Created: 22 Jan 2013 2:49:07pm
    Author:  tj3-mitchell

  ==============================================================================
*/

#include "FilePlayerGui.h"

FilePlayerGui::FilePlayerGui (FilePlayer& filePlayer_) : filePlayer (filePlayer_)

{
    playButton.setButtonText (">");
    playButton.addListener(this);
    addAndMakeVisible(&playButton);
    
    AudioFormatManager formatManager;
    formatManager.registerBasicFormats();
    fileChooser = new FilenameComponent ("audiofile",
                                         File::nonexistent,
                                         true, false, false,
                                         formatManager.getWildcardForAllFormats(),
                                         String::empty,
                                         "(choose a WAV or AIFF file)");
    fileChooser->addListener(this);
    addAndMakeVisible(fileChooser);
    
    //Slider
    playbackPosSlider.setSliderStyle(Slider::LinearHorizontal);
    playbackPosSlider.setRange (0.0,1.0);
    addAndMakeVisible(playbackPosSlider);
    playbackPosSlider.addListener(this);

    //Pitch Slider
    pitchSlider.setSliderStyle(Slider::LinearHorizontal);
    pitchSlider.setRange (0.01,5.0);
    pitchSlider.setValue(1.0);
    addAndMakeVisible(pitchSlider);
    pitchSlider.addListener(this);
    
}

FilePlayerGui::~FilePlayerGui()
{
    
}

//Component
void FilePlayerGui::resized()
{
    playButton.setBounds (0, 0, getHeight(), 20);
    fileChooser->setBounds (getHeight(), 0, getWidth()-getHeight(), 20);
    playbackPosSlider.setBounds (0, 20, getWidth() - 20, 40);
    pitchSlider.setBounds (0, 40, getWidth() - 20, 40);
    
}

void FilePlayerGui::paint (Graphics& g)
{
    //Rectangle <int> wall (100,100,50,50);
}

//Button Listener
void FilePlayerGui::buttonClicked (Button* button)
{
        if (button == &playButton)
    {
        filePlayer.setPlaying(!filePlayer.isPlaying());
    }
}

//FilenameComponentListener
void FilePlayerGui::filenameComponentChanged (FilenameComponent* fileComponentThatHasChanged)
{
    if (fileComponentThatHasChanged == fileChooser)
    {
        File audioFile (fileChooser->getCurrentFile().getFullPathName());
        
        if(audioFile.existsAsFile())
        {
                filePlayer.loadFile(audioFile);
        }
        else
        {
            AlertWindow::showMessageBox (AlertWindow::WarningIcon,
                                         "sdaTransport",
                                         "Couldn't open file!\n\n");
        }
    }
}

//slider Listener
void FilePlayerGui::sliderValueChanged(Slider* slider)
{
    if (slider == &playbackPosSlider)
    {
    //DBG ("Slider value = \n" << playbackPosSlider.getValue());
    filePlayer.setPosition(playbackPosSlider.getValue());
    }
    else if (slider == &pitchSlider)
    {
    filePlayer.setPlaybackRate(pitchSlider.getValue());
    }
    
}
