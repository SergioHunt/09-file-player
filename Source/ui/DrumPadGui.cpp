//
//  DrumPadGui.cpp
//  JuceBasicAudio - App
//
//  Created by Sergio Hunt on 13/12/2017.
//

#include "DrumPadGui.hpp"

DrumPadGui::DrumPadGui(FilePlayer& filePlayer_)
{
    //drumPad[0].setButtonText (">");
    

    for (int n=0; n <= 15; n++)
    {
        addAndMakeVisible(drumPad[n]);
        drumPad[n].addListener(this);
    }
}

DrumPadGui::~DrumPadGui()
{
    
}

void DrumPadGui::resized()
{
    

    auto r = getLocalBounds();
    
    int Sidelength = 90;
    auto row1 = r.removeFromTop(Sidelength);
    auto row2 = r.removeFromTop(Sidelength);
    auto row3 = r.removeFromTop(Sidelength);
    auto row4 = r.removeFromTop(Sidelength);
    
    
   
    
    for (int n=0; n <= 3; n++)
    {
        drumPad[n].setBounds(row1.removeFromLeft(Sidelength).reduced(5));
    }
    
    for (int n=4; n <= 7; n++)
    {
        drumPad[n].setBounds(row2.removeFromLeft(Sidelength).reduced(5));
    }
    
    for (int n=8; n <= 11; n++)
    {
        drumPad[n].setBounds(row3.removeFromLeft(Sidelength).reduced(5));
    }
    for (int n=12; n <= 15; n++)
    {
        drumPad[n].setBounds(row4.removeFromLeft(Sidelength).reduced(5));
    }
}

void DrumPadGui::paint (Graphics& g)
{
   // DBG ("Paint Function is being Called");
    auto r = getLocalBounds();
    g.setColour(Colours::grey);
    g.fillRect(r);
    
    g.drawRect(r);
    
    
}

void DrumPadGui::buttonClicked (Button* button)
{
    if (button == &drumPad[0])
    {
        DBG ("Eatshit");
        //filePlayer.setPlaying(!filePlayer.isPlaying());
    }
    else if (button == &drumPad[1])
    {
        DBG ("GetFucked");
    }
}


