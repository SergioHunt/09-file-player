//
//  WaveFormGui.hpp
//  JuceBasicAudio - App
//
//  Created by Sergio Hunt on 13/12/2017.
//

#ifndef WaveFormGui_hpp
#define WaveFormGui_hpp

#include "../JuceLibraryCode/JuceHeader.h"
#include <stdio.h>

class WaveFormGui : public Component //public AudioVisualiserComponent

{
public:
    /**
     constructor -
     */
    WaveFormGui();
    
    /**
     Destructor
     */
    ~WaveFormGui();
    
    //AudioVisualiser
    //setNumChannels();
    
    //Component
    void resized() override;
    void paint (Graphics& g) override;
    
//    void paintChannel(    Graphics &     ,
//                      Rectangle< float >     bounds,
//                      const Range< float > *     levels,
//                      int     numLevels,
//                      int     nextSample
//                      ) override;


private:
     AudioFormatManager formatManager;         
    AudioThumbnailCache thumbnailCache;
    AudioThumbnail thumbnail;  


};
#endif /* WaveFormGui_hpp */

