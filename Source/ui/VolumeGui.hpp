//
//  VolumeGui.hpp
//  JuceBasicAudio - App
//
//  Created by Sergio Hunt on 28/12/2017.
//

#ifndef VolumeGui_hpp
#define VolumeGui_hpp

#include "../JuceLibraryCode/JuceHeader.h"
#include <stdio.h>

class VolumeGui :   public Component,
                    public Slider::Listener
{
public:
     VolumeGui();
    
    ~VolumeGui();
    
    //Component
    void resized() override;
    void paint (Graphics& g) override;
    
    //Slider Listener
    void sliderValueChanged(Slider* slider)override ;
    
    
private:
    Slider masterVolume;
    Slider recordVolume;
    
};

#endif /* VolumeGui_hpp */
