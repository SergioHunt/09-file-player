//
//  DrumPad.hpp
//  JuceBasicAudio - App
//
//  Created by Sergio Hunt on 13/12/2017.
//

#ifndef DrumPad_hpp
#define DrumPad_hpp

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"

class DrumPad : public AudioSource,
public ChangeListener,
public Button::Listener
{
public:
    //**Constructor*/
    DrumPad();
    /**
     Destructor
     */
    ~DrumPad();
    
    
private:
    
    enum TransportState
    {
        Stopped,
        Starting,
        Playing,
        Stopping
    };
    
    AudioFormatManager formatManager;
    
    AudioTransportSource transportSource;
    
    ScopedPointer<AudioFormatReaderSource> readerSource;
    
    TransportState state;
};

#endif /* DrumPad_hpp */

